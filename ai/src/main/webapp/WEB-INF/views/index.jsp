<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title>Form</title>
</head>
<body>
    <h1>Simple Text Form</h1>
    <form action="${pageContext.request.contextPath}/gpt3" method="post">
        <label for="textValue">Text:</label>
        <input type="text" id="message" name="message" />
        <button type="submit">Submit</button>
    </form>
    <h1>Result</h1>
    <p>Text value submitted: ${response}</p>
</body>
</html>
