package it.prismaprogetti.ai;

public record CompletionRequest(String model, String prompt, double temperature, int maxTokens) {

	public static CompletionRequest defaultWith(String prompt) {

		return new CompletionRequest("text-davinci-003", prompt, 0.7, 100);
	}
}
